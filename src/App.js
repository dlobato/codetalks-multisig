import React from 'react';
import { Section, Container } from 'bloomer';
import {
    BrowserRouter as Router,
    Route,
    Link 
} from 'react-router-dom'

import './css/App.css';
//import logo from './logo.svg';

import CreatePage from './pages/CreatePage';
import GroupEditor from './pages/GroupEditor';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = props;
    } 

    setGroup(group) {

        this.setState({
            sharing_group: group,
        });
    }

    render() {
        const sharingGroupLink = this.state.sharing_group.name ?
        <Link to="/sharing-group" >sharing group</Link>
        : ''
        return (
            <Router>
                <Section>
                    <Container isFluid>
                        <Link to="/create" >create</Link> | 
                        
                        {sharingGroupLink}

                        <Route exact path="/create" render={(props) => 
                            <CreatePage onGroupCreated={this.setGroup.bind(this)} />
                        }/>
                        <Route exact path="/sharing-group" render={(props) => <GroupEditor group={this.state.sharing_group} />} />
                        
                    </Container>
                </Section>
            </Router>
        );
    }
}

export default App;