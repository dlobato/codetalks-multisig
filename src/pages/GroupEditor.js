import React from 'react';
import { Field, Label, Control, Input, Button } from 'bloomer';
import AceEditor from 'react-ace';
import MultisigWallet from '../components/Wallet';
import 'brace/mode/json';
import 'brace/theme/github';

class GroupEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            seed: '',
            name: props.group.wallet && props.group.wallet.parties.length == 0 ? props.group.owner : '',
            group: props.group,
        }
        
        this.editorContent = JSON.stringify(this.state.group || {}, null, '  ');

        this.handleJsonChanged = this.handleJsonChanged.bind(this);
    }
    
    handleJsonChanged(val) {
        this.editorContent = val;
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleSeedChange(event) {
        this.setState({seed: event.target.value});
    }

    addSigner() {
        const member = {
            seed: this.state.seed,
            name: this.state.name,
        }

        const wallet = new MultisigWallet(this.state.group.wallet);
        wallet.addMember(1, member);
        this.setState({
            wallet: wallet.data
        })
    }
    
    finalize() {
        const wallet = new MultisigWallet(this.state.group.wallet);
        wallet.finalize();
        this.setState({
            wallet: wallet.data
        })
    }

    updateFromJson() {
        const newState = { group: JSON.parse(this.editorContent) };
        this.setState(newState);
        if (newState.group.wallet.parties.length == 0) {
            this.setState({name: newState.group.owner})
        }
    }

    generateSeed() {
        const seedsize = 81;
        const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ9";
        let seed = "";
        for (var i = 0, n = chars.length; i < seedsize; ++i) {
            seed += chars.charAt(Math.floor(Math.random() * n));
        }
        this.setState({ seed })
    }

    amIAlreadyAParty() {
        if (!this.state.group.wallet)
            return false;
        
        const inThere = this.state.group.wallet.parties.filter(p => p.id == this.state.name);
        return inThere.length > 0;

    }

    render() {

        const json = JSON.stringify(this.state.group || {}, null, '  ');
        
        const addButton = (this.state.name && this.state.seed) ?
                <Button isColor='primary' onClick={this.addSigner.bind(this)}>Add</Button>
            : ''
        
        const finalizeButton = (this.state.group.wallet && this.state.group.wallet.parties.length > 1) ?

                <Control>
                <Button isColor='warning' onClick={this.finalize.bind(this)}>finalize</Button>
                </Control>
        : ''
    
        const addControl = this.amIAlreadyAParty() ? 
                <Label>{this.state.name}, please note your seed is: {this.state.seed}</Label>

            :
                <Control>
                    <Input placeholder='Your name' value={this.state.name} onChange={this.handleNameChange.bind(this)} />
                    <Input placeholder='Your seed' value={this.state.seed} onChange={this.handleSeedChange.bind(this)} />
                    
                    <Button isColor='primary' onClick={this.generateSeed.bind(this)}>Generate Seed</Button>
                    {addButton}
                    
                </Control>
            ;
        
        return (
            <div>
                <Field>
                    <Label>Edit configuration</Label>
                    <Control>
                        <AceEditor
                            mode="json"
                            theme="github"
                            height="500px"
                            width="100%"
                            onChange={this.handleJsonChanged}
                            name="UNIQUE_ID_OF_DIV"
                            value={json}
                            editorProps={{$blockScrolling: true}}
                        />

                    </Control>
                </Field>

                <Field>
                    <Control>
                        <Button isColor='primary' onClick={this.updateFromJson.bind(this)}>Update</Button>
                    </Control>
                    
                    {addControl}

                    {finalizeButton}

                </Field>
                
            </div>
        );
    }
}

export default GroupEditor;
