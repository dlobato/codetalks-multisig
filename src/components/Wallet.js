const IOTA = require('iota.lib.js');

var iota = new IOTA({
    'host': 'http://node02.iotatoken.nl',
    'port': 14265
});

const Address = iota.multisig.address;

const security = 2;

var appState = {
    members: {}
}

export default class MultisigWallet {

    constructor(data) {
        if ('undefined' === typeof data) {
            this.data = {
                parties: []
            };    
        } else {
            this.data = data;
        }
    }
    
    finalize() {
        const digests = this.data.parties.map(p => p.digest);
        //absorb digests
        const address = new Address()
        // absorb digests
        .absorb(digests)
        //and finally we finalize the address itself
        .finalize();
        
        this.data.address = address;
        
        return address;

    }

    addMember(index, member) {
        
        const digest = iota.multisig.getDigest(member.seed, index, security);
        const party =  {
            id: member.name,
            security,
            indexMain: index,
            digest
        };
        this.data.parties.push(party);
    }
}

/*
var members = parsedOpts.members.split(',')
    .map((i) => i.trim())
    .map((ii) => appState.members.find((x) => (x.id === ii)))


var digests = members.map((m) => iota.multisig.getDigest(m.seed, 0, parsedOpts.security)); //index 0


var addressWithChecksum = iota.utils.addChecksum(address, 9, true);

console.log("MULTISIG ADDR+CC: ", addressWithChecksum);

// Simple validation if the multisig was created correctly
// Can be called by each cosigner independently
var isValid = iota.multisig.validateAddress(address, digests);

console.log("IS VALID MULTISIG ADDRESS:", isValid);

*/