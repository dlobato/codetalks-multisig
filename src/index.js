import React from 'react';
import ReactDOM from 'react-dom';

import './css/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const globalState = {
    sharing_group: {}
};

ReactDOM.render(
    <App {...globalState}/>
, document.getElementById('root'));
registerServiceWorker();
